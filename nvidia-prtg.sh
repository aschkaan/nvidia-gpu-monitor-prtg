#!/bin/bash
#Performance State (P0:max-P12:min)
b1=$(nvidia-smi -q --display=PERFORMANCE | grep "Performance State" | cut -d ':' -f 1)
b2=$(nvidia-smi -q --display=PERFORMANCE | grep "Performance State" | cut -d ':' -f 2 | tr -dc '0-9')
#GPU Current Temp
c1=$(nvidia-smi -q -d temperature | grep "GPU Current Temp" | cut -d ':' -f 1)
c2=$(nvidia-smi -q -d temperature | grep "GPU Current Temp" | cut -d ':' -f 2 | tr -dc '0-9')
#GPU usage (%)
d1=$(nvidia-smi -q --display=UTILIZATION | grep Gpu | cut -d ':' -f 1)
d2=$(nvidia-smi -q --display=UTILIZATION | grep Gpu | cut -d ':' -f 2 | tr -dc '0-9')
#Memory usage (%)
e1=$(nvidia-smi -q --display=UTILIZATION | grep "Memory" | head -1 | cut -d ':' -f 1)
e2=$(nvidia-smi -q --display=UTILIZATION | grep "Memory" | head -1 | cut -d ':' -f 2 | tr -dc '0-9')
echo "<prtg>
        <result>
                <channel>$b1</channel>
                <value>$b2</value>
        </result>
        <result>
                <channel>$c1</channel>
                <value>$c2</value>
        </result>
        <result>
                <channel>$d1</channel>
                <value>$d2</value>
        </result>
        <result>
                <channel>$e1</channel>
                <value>$e2</value>
        </result>
</prtg>"
