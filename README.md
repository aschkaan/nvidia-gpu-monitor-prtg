**About**

This is a [SSH script advanced sensor](https://www.paessler.com/manuals/prtg/ssh_script_advanced_sensor) for monitoring [Nvidia](https://www.nvidia.com/) GPU.

**Prerequisites**

At first, you should [enable monitor remote machines via SSH](https://www.paessler.com/manuals/prtg/ssh_monitoring) in prtg.

**Install**

`wget -c https://gitlab.com/aschkaan/nvidia-gpu-monitor-prtg/-/archive/master/nvidia-gpu-monitor-prtg-master.tar.gz`

`tar -xzvf nvidia-gpu-monitor-prtg-master.tar.gz`

`cp nvidia-gpu-monitor-prtg-master/nvidia-prtg.sh /var/prtg/scriptsxml`

`chmod a+x /var/prtg/scriptsxml/nvidia-prtg.sh`
